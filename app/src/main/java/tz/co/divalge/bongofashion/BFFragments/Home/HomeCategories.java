package tz.co.divalge.bongofashion.BFFragments.Home;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import tz.co.divalge.bongofashion.BFActivites.MainActivity;
import tz.co.divalge.bongofashion.BFAdapters.CategoryAdapter;
import tz.co.divalge.bongofashion.BFHelpers.BFToast;
import tz.co.divalge.bongofashion.BFModels.CategoryModel;
import tz.co.divalge.bongofashion.BFValues.APIURls;
import tz.co.divalge.bongofashion.BFValues.BFConstants;
import tz.co.divalge.bongofashion.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeCategories extends Fragment implements SwipeRefreshLayout.OnRefreshListener  {

    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;

    public HomeCategories() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_home_categories, container, false);
        recyclerView = view.findViewById(R.id.home_categories_recyclerview);

        swipeRefreshLayout = view.findViewById(R.id.home_categories_swiperefreshlayout);
        swipeRefreshLayout.setOnRefreshListener(this);

        loadData();
        return view;
    }

    private void loadData(){
        swipeRefreshLayout.setRefreshing(true);
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.mainActivityContext);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                APIURls.USERCATS,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        fillRecyclerView(response);
                    }
                },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                BFToast asdf = new BFToast(error.getMessage());
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        jsonArrayRequest.setShouldCache(false);
        requestQueue.add(jsonArrayRequest);
    }

    private void fillRecyclerView(JSONArray response) {
            List<CategoryModel> categories = new ArrayList<>();

            try{
                for(int i = 0;i<response.length();i++){
                    JSONObject j = response.getJSONObject(i);
                    CategoryModel category = new CategoryModel();
                    category.Name = j.getString("name")+"s";
                    category.Id = Integer.parseInt(j.getString("id"));
                    category.ImageLink = j.getString("image");
                    categories.add(category);
                }

                CategoryAdapter postsAdapter = new CategoryAdapter(categories,getActivity().getBaseContext(),getActivity(), BFConstants.CATEGORIES_VALUE);
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity().getBaseContext(),1);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(postsAdapter);
                swipeRefreshLayout.setRefreshing(false);

            }catch(JSONException j){

            }
    }

    @Override
    public void onRefresh() {
        loadData();
    }
}
