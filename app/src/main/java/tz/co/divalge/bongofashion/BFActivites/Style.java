package tz.co.divalge.bongofashion.BFActivites;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import tz.co.divalge.bongofashion.BFAdapters.PostsAdapter;
import tz.co.divalge.bongofashion.BFHelpers.BFToast;
import tz.co.divalge.bongofashion.BFModels.PostList;
import tz.co.divalge.bongofashion.BFValues.APIURls;
import tz.co.divalge.bongofashion.R;

public class Style extends AppCompatActivity  implements SwipeRefreshLayout.OnRefreshListener {
    private String url;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_style);

        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        url = intent.getStringExtra("url");
        TextView styleTitle = findViewById(R.id.style_title);
        styleTitle.setText(name);

        recyclerView = findViewById(R.id.style_recyclerview);
        swipeRefreshLayout = findViewById(R.id.style_swiperefreshlayout);

        swipeRefreshLayout.setOnRefreshListener(this);
        loadData();
    }

    private void loadData(){
        swipeRefreshLayout.setRefreshing(true);
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.mainActivityContext);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        fillRecyclerView(response);
                    }
                },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                BFToast asdf = new BFToast(error.getMessage());
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        jsonArrayRequest.setShouldCache(false);
        requestQueue.add(jsonArrayRequest);
    }


    private void fillRecyclerView(JSONArray jsonArray) {
        List<PostList> postLists = new ArrayList<>();
        try{
            for(int i=0; i<jsonArray.length();i++){
                JSONObject j = jsonArray.getJSONObject(i);
                PostList postList = new PostList();
                postList.name = j.getString("name");
                postList.postedBy = j.getString("posted_by");
                postList.postTime  = j.getString("time");
                postList.postImage = j.getString("image");
                postList.Id = Integer.parseInt(j.getString("id"));
                postLists.add(postList);
            }

            PostsAdapter postsAdapter = new PostsAdapter(postLists,getApplication().getApplicationContext(),this);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this,2);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(postsAdapter);
            swipeRefreshLayout.setRefreshing(false);
        }catch (JSONException j){
            Log.e("exception",j.getMessage());
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public  void onBackPressed(){
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in,R.anim.slide_down);
    }

    @Override
    public void onRefresh() {
        loadData();
    }
}
