package tz.co.divalge.bongofashion.BFFragments.Home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import tz.co.divalge.bongofashion.BFActivites.MainActivity;
import tz.co.divalge.bongofashion.BFAdapters.StyleAdapter;
import tz.co.divalge.bongofashion.BFHelpers.BFToast;
import tz.co.divalge.bongofashion.BFModels.Style;
import tz.co.divalge.bongofashion.BFValues.APIURls;
import tz.co.divalge.bongofashion.BFValues.BFConstants;
import tz.co.divalge.bongofashion.R;

public class HomeSuperCats extends Fragment  implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;

    public HomeSuperCats() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_home_super_cats, container, false);
        recyclerView = view.findViewById(R.id.home_supercats_recyclerview);

        swipeRefreshLayout = view.findViewById(R.id.home_supercats_swiperefreshlayout);
        swipeRefreshLayout.setOnRefreshListener(this);

        loadData();
        return view;
    }

    private void loadData(){
        swipeRefreshLayout.setRefreshing(true);
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.mainActivityContext);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                APIURls.SUPERCATS,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        fillRecyclerView(response);
                    }
                },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                BFToast asdf = new BFToast(error.getMessage());
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        jsonArrayRequest.setShouldCache(false);
        requestQueue.add(jsonArrayRequest);
    }

    private void fillRecyclerView(JSONArray response) {

        List<Style> styles = new ArrayList<>();
        try{
            for(int i = 0;i<response.length();i++){
                JSONObject j = response.getJSONObject(i);
                Style style = new Style();
                style.Name = j.getString("name");
                style.Id = Integer.parseInt(j.getString("id"));
                style.ImageLink = j.getString("image");
                styles.add(style);
            }

            StyleAdapter postsAdapter = new StyleAdapter(styles,getActivity().getBaseContext(),getActivity(), BFConstants.SUPERCATS_VALUE);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity().getBaseContext(),2);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(postsAdapter);
            swipeRefreshLayout.setRefreshing(false);

        }catch(JSONException j){

        }
    }

    @Override
    public void onRefresh() {
        loadData();
    }
}
