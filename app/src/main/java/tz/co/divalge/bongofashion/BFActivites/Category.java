package tz.co.divalge.bongofashion.BFActivites;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import tz.co.divalge.bongofashion.BFAdapters.MainPagerAdapter;
import tz.co.divalge.bongofashion.BFFragments.Category.LatestPosts;
import tz.co.divalge.bongofashion.BFFragments.Category.CategoryLists;
import tz.co.divalge.bongofashion.R;

public class Category extends AppCompatActivity {


    private TabLayout tabLayout;
    private ViewPager viewPager;
    public static int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        Intent intent = getIntent();
        id=intent.getIntExtra("id",0);

        tabLayout = findViewById(R.id.category_tablayout);
        viewPager = findViewById(R.id.category_viewpager);
        initialiseViewPager();
    }

    @Override
    public  void onBackPressed(){
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in,R.anim.slide_down);
    }

    private void initialiseViewPager() {
        MainPagerAdapter mainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        mainPagerAdapter.addFragment(new LatestPosts());
        mainPagerAdapter.addFragment(new CategoryLists());

        viewPager.setAdapter(mainPagerAdapter);
        viewPager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
    }
}
