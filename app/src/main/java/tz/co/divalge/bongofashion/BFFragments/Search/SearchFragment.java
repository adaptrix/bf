package tz.co.divalge.bongofashion.BFFragments.Search;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonArrayRequest;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import tz.co.divalge.bongofashion.BFActivites.MainActivity;
import tz.co.divalge.bongofashion.BFAdapters.PostsAdapter;
import tz.co.divalge.bongofashion.BFHelpers.BFToast;
import tz.co.divalge.bongofashion.BFModels.PostList;
import tz.co.divalge.bongofashion.BFValues.APIURls;
import tz.co.divalge.bongofashion.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends DialogFragment implements  Response.ErrorListener, Response.Listener<JSONObject>
,View.OnClickListener{

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private RequestQueue requestQueue;
    private Spinner userfilter,stylefilter,supercatfilter;
    private Button searchButton;
    private LinearLayout searchFilters,searchLoading;
    private String StyleId,UserId,SuperCatId;
    private EditText searchQuery;
    final List<String> stylesList = new ArrayList<>(),usercatsList = new ArrayList<>(),supercatsList = new ArrayList<>();
    final List<String> stylesIdList = new ArrayList<>(),usercatsIdList = new ArrayList<>(),supercatsIdList = new ArrayList<>();

    public SearchFragment() {
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search, container, false);
        recyclerView = v.findViewById(R.id.search_recyclerview);
        progressBar = v.findViewById(R.id.search_progressbar);
        userfilter = v.findViewById(R.id.search_user_spinner);
        stylefilter = v.findViewById(R.id.search_style_spinner);
        supercatfilter = v.findViewById(R.id.search_super_spinner);
        searchButton = v.findViewById(R.id.search_button);
        searchFilters = v.findViewById(R.id.search_filters);
        searchLoading = v.findViewById(R.id.search_loading);
        searchQuery = v.findViewById(R.id.search_query);

        searchButton.setOnClickListener(this);
        fillSpinners();
        //loadRecyclerViewData();
        return v;
    }

    private void fillSpinners(){
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.mainActivityContext);
        String url = APIURls.FILTERS;
        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.GET, url,null, this,this);

        jsonArrayRequest.setShouldCache(false);
        requestQueue.add(jsonArrayRequest);
    }

    @Override
    public void onResponse(JSONObject response) {
        try{
            JSONArray styles = response.getJSONArray("styles");
            JSONArray usercats = response.getJSONArray("usercats");
            JSONArray supercats = response.getJSONArray("supercats");
            stylesList.add("Style");
            stylesIdList.add("0");
            for(int i=0; i<styles.length(); i++){
                JSONObject jo = styles.getJSONObject(i);
                stylesList.add(jo.getString("name"));
                stylesIdList.add(jo.getString("id"));
            }
            usercatsList.add("Category");
            usercatsIdList.add("0");
            for(int i=0; i<usercats.length();i++){
                JSONObject jo = usercats.getJSONObject(i);
                usercatsList.add(jo.getString("catName"));
                usercatsIdList.add(jo.getString("id"));
            }
            supercatsList.add("Type");
            supercatsIdList.add("0");
            for(int i=0;i<supercats.length();i++){
                JSONObject jo = supercats.getJSONObject(i);
                supercatsList.add(jo.getString("name"));
                supercatsIdList.add(jo.getString("id"));
            }


            String[] names = stylesList.toArray(new String[stylesList.size()]);
            ArrayAdapter arrayAdapter =
                    new ArrayAdapter(MainActivity.mainActivityContext,android.R.layout.simple_spinner_item,names);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            stylefilter.setAdapter(arrayAdapter);

            String[] namesSuperCat = supercatsList.toArray(new String[supercatsList.size()]);
            ArrayAdapter arrayAdapter1 =
                    new ArrayAdapter(MainActivity.mainActivityContext,android.R.layout.simple_spinner_item,namesSuperCat);
            arrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            supercatfilter.setAdapter(arrayAdapter1);



            String[] namesUsers = usercatsList.toArray(new String[usercatsList.size()]);
            ArrayAdapter arrayAdapter2 =
                    new ArrayAdapter(MainActivity.mainActivityContext,android.R.layout.simple_spinner_item,namesUsers);
            arrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            userfilter.setAdapter(arrayAdapter2);


            searchLoading.setVisibility(View.GONE);
            searchFilters.setVisibility(View.VISIBLE);

            //stylefilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            //    @Override
            //    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            //        StyleId=stylesIdList.get(position);
            //    }
//
            //    @Override
            //    public void onNothingSelected(AdapterView<?> parent) {
//
             ///   }
            //});

        }catch (JSONException j){
            BFToast fasdf = new BFToast(j.getMessage());
        }
    }

    private void fillRecyclerView(JSONArray response) {

        List<PostList> postLists = new ArrayList<>();
        try{
            for(int i = 0;i<response.length();i++){
                PostList postList = new PostList();
                JSONObject j = response.getJSONObject(i);
                postList.name = j.getString("name");
                postList.postedBy = j.getString("posted_by");
                postList.postTime  = j.getString("time");
                postList.postImage = j.getString("image");
                postList.Id = Integer.parseInt(j.getString("id"));
                postLists.add(postList);
            }
            PostsAdapter postsAdapter = new PostsAdapter(postLists,getActivity().getBaseContext(),getActivity());
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity().getBaseContext(),2);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(postsAdapter);
            progressBar.setVisibility(ProgressBar.GONE);


        }catch (JSONException j){
            BFToast asdf = new BFToast(j.getMessage());
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onResume() {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
        super.onResume();
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.search_button:
                postData();
                break;
        }
    }

    private void postData() {
        String text = searchQuery.getText().toString();
        if(text.equals("")||text==null){BFToast fasdfasdf = new BFToast("type something to search...");}
        else{
            progressBar.setVisibility(View.VISIBLE);
            String url = APIURls.SEARCH + "?search_term="+text;

            if(userfilter.getSelectedItemPosition()!=0)
                url = url + "&type=" + usercatsIdList.get(userfilter.getSelectedItemPosition());
            if(supercatfilter.getSelectedItemPosition()!=0)
                url = url + "&supercat=" + supercatsIdList.get(supercatfilter.getSelectedItemPosition());
            if(stylefilter.getSelectedItemPosition() !=0)
                url = url + "&style="+ stylesIdList.get(stylefilter.getSelectedItemPosition());

            requestQueue = Volley.newRequestQueue(MainActivity.mainActivityContext);
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,
                    url,
                    null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            fillRecyclerView(response);
                        }
                    },  new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.GONE);
                    BFToast asdf = new BFToast("3wer4"+error.networkResponse.statusCode);
                }
            });
            jsonArrayRequest.setShouldCache(false);
            requestQueue.add(jsonArrayRequest);
        }
    }
}
