package tz.co.divalge.bongofashion.BFAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import tz.co.divalge.bongofashion.BFHelpers.BFImageLoad;
import tz.co.divalge.bongofashion.BFModels.User;
import tz.co.divalge.bongofashion.BFModels.UserList;
import tz.co.divalge.bongofashion.BFValues.APIURls;
import tz.co.divalge.bongofashion.R;

/**
 * Created by Mwakalinga J on 2/24/2018.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> implements View.OnClickListener {
    private List<UserList> users;
    private Context context;
    private Activity activity;

    public class UserViewHolder extends RecyclerView.ViewHolder{
        ImageView userImage;
        CardView userContainer;
        TextView  userName,userNubmerOfPosts;

        public UserViewHolder(View itemView) {
            super(itemView);
            userContainer = itemView.findViewById(R.id.user_container);
            userImage =  itemView.findViewById(R.id.user_image);
            userName = itemView.findViewById(R.id.user_name);
            userNubmerOfPosts = itemView.findViewById(R.id.user_number_of_posts);
        }
    }

    public UserAdapter(List<UserList> u, Context c, Activity a){
        this.users = u;
        context=c;
        activity = a;
    }

    @Override
    public UserAdapter.UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user,parent,false);
        return new UserAdapter.UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UserAdapter.UserViewHolder holder, int position) {
        final UserList user = users.get(position);
        holder.userName.setText(user.Name);
        holder.userNubmerOfPosts.setText(user.PostCount);
        BFImageLoad asdfa = new BFImageLoad(holder.userImage, user.ImageLink,context);
        holder.userContainer.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.user_container:
                Intent intent = new Intent(context, tz.co.divalge.bongofashion.BFActivites.Profile.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.slide_up,R.anim.fade_out);
                break;
        }
    }
}
