package tz.co.divalge.bongofashion.BFActivites;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonArrayRequest;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import tz.co.divalge.bongofashion.BFAdapters.PostsAdapter;
import tz.co.divalge.bongofashion.BFHelpers.BFImageLoad;
import tz.co.divalge.bongofashion.BFHelpers.BFToast;
import tz.co.divalge.bongofashion.BFModels.PostList;
import tz.co.divalge.bongofashion.BFValues.APIURls;
import tz.co.divalge.bongofashion.R;

public class Profile extends AppCompatActivity {
    private boolean visible = true;
    private int userId;
    private String name;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private RequestQueue requestQueue;

    private TextView profileName,profileType,profilePhone,profileEmail,profileAddress,profileBio,profileNumberOfPosts;
    private ImageView profileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Intent intent = getIntent();
        userId = intent.getIntExtra("id",0);

        recyclerView = findViewById(R.id.profile_products_recyclerview);
        progressBar = findViewById(R.id.profile_progressBar);

        profileName = findViewById(R.id.profile_name);
        profileAddress = findViewById(R.id.profile_address);
        profileType = findViewById(R.id.profile_type);
        profilePhone = findViewById(R.id.profile_phone);
        profileEmail = findViewById(R.id.profile_email);
        profileImage = findViewById(R.id.profile_image);
        profileBio = findViewById(R.id.profile_bio);
        profileNumberOfPosts = findViewById(R.id.profile_number_of_posts);

        recyclerView.setNestedScrollingEnabled(false);
        loadData();
    }

    @Override
    public  void onBackPressed(){
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in,R.anim.slide_down);
    }

    private void loadData(){
        progressBar.setVisibility(ProgressBar.VISIBLE);
        requestQueue = Volley.newRequestQueue(MainActivity.mainActivityContext);
        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.GET,
                APIURls.USER_PROFILE_USERID+userId,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        fillPostInfo(response);
                    }
                },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                BFToast asdf = new BFToast(error.getMessage());
            }
        });
        jsonArrayRequest.setShouldCache(false);
        requestQueue.add(jsonArrayRequest);
    }


    private void fillPostInfo(JSONObject response) {
        try{
            profileName.setText(response.getString("name"));
            profilePhone.setText(response.getString("phone"));
            profileEmail.setText(response.getString("email"));
            profileAddress.setText(response.getString("address"));
            profileBio.setText(response.getString("bio"));
            profileType.setText(response.getString("usercat"));
            profileNumberOfPosts.setText(response.getString("number_of_posts"));
            String imageUrl =response.getString("image");
            BFImageLoad fasdf =new BFImageLoad(profileImage,imageUrl,this,false);
            loadRecyclerViewData();

        }catch (JSONException j){
            BFToast fasdfas = new BFToast(this,j.getMessage());
        }
    }

    private void loadRecyclerViewData(){
        progressBar.setVisibility(ProgressBar.VISIBLE);
        requestQueue = Volley.newRequestQueue(MainActivity.mainActivityContext);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                APIURls.USERS_POSTS_USERID+userId,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        fillRecyclerView(response);
                    }
                },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                BFToast asdf = new BFToast(error.getMessage());
            }
        });
        jsonArrayRequest.setShouldCache(false);
        requestQueue.add(jsonArrayRequest);
    }


    private void fillRecyclerView(JSONArray response) {

        List<PostList> postLists = new ArrayList<>();
        try{
            for(int i = 0;i<response.length();i++){
                PostList postList = new PostList();
                JSONObject j = response.getJSONObject(i);
                postList.name = j.getString("name");
                postList.postedBy = j.getString("posted_by");
                postList.postTime  = j.getString("time");
                postList.postImage = j.getString("image");
                postList.Id = Integer.parseInt(j.getString("id"));
                postLists.add(postList);
            }
            PostsAdapter postsAdapter = new PostsAdapter(postLists,this,this);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this,2);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(postsAdapter);
            progressBar.setVisibility(ProgressBar.GONE);


        }catch (JSONException j){
            BFToast asdf = new BFToast(j.getMessage());
        }
    }

}
