package tz.co.divalge.bongofashion.BFActivites;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonArrayRequest;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import tz.co.divalge.bongofashion.BFAdapters.PostsAdapter;
import tz.co.divalge.bongofashion.BFHelpers.BFImageLoad;
import tz.co.divalge.bongofashion.BFHelpers.BFToast;
import tz.co.divalge.bongofashion.BFModels.PostList;
import tz.co.divalge.bongofashion.BFValues.APIURls;
import tz.co.divalge.bongofashion.R;

public class ViewPost extends AppCompatActivity {
    private boolean visible = true;
    private int post_id;
    private String name;
    private ProgressBar progressBar;
    private RecyclerView viewPostMoreFromRecyclerView;
    private RequestQueue requestQueue;

    private TextView viewPostName,viewPostDescription,viewPostStyle,viewPostDate,viewPostMoreFrom;
    private TextView viewPostNameBottomSheet,viewPostImageCount;
    private CarouselView carouselView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_post);

        Intent intent = getIntent();
        post_id = intent.getIntExtra("id",0);
        name = intent.getStringExtra("name");

        LinearLayout viewPostImageHolder = findViewById(R.id.view_post_image_holder);
        final LinearLayout viewPostTitleHolder = findViewById(R.id.view_post_title_holder);
        final LinearLayout viewPostBottomSheet = findViewById(R.id.view_post_bottomsheet);
        viewPostMoreFromRecyclerView = findViewById(R.id.view_post_more_from_recycler_view);
        progressBar = findViewById(R.id.view_post_progressbar);
        viewPostName = findViewById(R.id.view_post_name);
        viewPostDescription = findViewById(R.id.view_post_description);
        viewPostStyle = findViewById(R.id.view_post_style);
        viewPostDate = findViewById(R.id.view_post_post_date);
        viewPostMoreFrom = findViewById(R.id.view_post_more_from);
        viewPostNameBottomSheet = findViewById(R.id.view_post_name_bottomsheet);
        viewPostImageCount = findViewById(R.id.view_post_count);
        carouselView = findViewById(R.id.carouselView);

        viewPostName.setText(name);
        viewPostMoreFromRecyclerView.setNestedScrollingEnabled(false);
        loadData();


        carouselView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(visible){
                    viewPostBottomSheet.setVisibility(View.GONE);
                    viewPostTitleHolder.setVisibility(View.GONE);
                    visible = false;
                }
                else{
                    viewPostBottomSheet.setVisibility(View.VISIBLE);
                    viewPostTitleHolder.setVisibility(View.VISIBLE);
                    visible = true;
                }
            }
        });
    }

    private void loadData(){
        progressBar.setVisibility(ProgressBar.VISIBLE);
        requestQueue = Volley.newRequestQueue(MainActivity.mainActivityContext);
        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.GET,
                APIURls.POST_DETAILS_POSTID+post_id,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        fillPostInfo(response);
                    }
                },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                BFToast asdf = new BFToast(error.getMessage());
            }
        });
        jsonArrayRequest.setShouldCache(false);
        requestQueue.add(jsonArrayRequest);
    }

    private void fillPostInfo(JSONObject response) {
        try{
            int user_id;
            final int image_count = Integer.parseInt(response.getString("image_count"));
            final List<String> images = new ArrayList<>();
            user_id = response.getInt("user_id");
            viewPostNameBottomSheet.setText(response.getString("name"));
            viewPostDescription.setText(response.getString("description"));
            viewPostDate.setText(response.getString("time"));
            viewPostMoreFrom.setText(response.getString("posted_by"));
            viewPostStyle.setText(response.getString("style"));
            viewPostImageCount.setText(response.getString("image_count"));
            JSONArray j = response.getJSONArray("images");
            for(int i=0; i<image_count;i++){
                JSONObject jsonObject = j.getJSONObject(i);
                images.add(jsonObject.getString("image_url"));
            }
            //BFToast asdfasd= new BFToast(images.get(0));
            loadRecyclerViewData(user_id);
            carouselView.setImageListener(new ImageListener() {
                @Override
                public void setImageForPosition(final int position, ImageView imageView) {
                    BFImageLoad sdf = new BFImageLoad(imageView,imageView.getContext(),images.get(position));
                }
            });
            carouselView.setPageCount(image_count);

        }catch (JSONException j){
            BFToast fasdfas = new BFToast(this,j.getMessage());
        }
    }

    private void loadRecyclerViewData(int user_id){
        progressBar.setVisibility(ProgressBar.VISIBLE);
        requestQueue = Volley.newRequestQueue(MainActivity.mainActivityContext);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                APIURls.USERS_POSTS_USERID+user_id,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        fillRecyclerView(response);
                    }
                },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                BFToast asdf = new BFToast(error.getMessage());
            }
        });
        jsonArrayRequest.setShouldCache(false);
        requestQueue.add(jsonArrayRequest);
    }


    private void fillRecyclerView(JSONArray response) {

        List<PostList> postLists = new ArrayList<>();
        try{
            for(int i = 0;i<response.length();i++){
                PostList postList = new PostList();
                JSONObject j = response.getJSONObject(i);
                postList.name = j.getString("name");
                postList.postedBy = j.getString("posted_by");
                postList.postTime  = j.getString("time");
                postList.postImage = j.getString("image");
                postList.Id = Integer.parseInt(j.getString("id"));
                postLists.add(postList);
            }
            PostsAdapter postsAdapter = new PostsAdapter(postLists,this,this);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this,2);
            viewPostMoreFromRecyclerView.setLayoutManager(mLayoutManager);
            viewPostMoreFromRecyclerView.setItemAnimator(new DefaultItemAnimator());
            viewPostMoreFromRecyclerView.setAdapter(postsAdapter);
            progressBar.setVisibility(ProgressBar.GONE);


        }catch (JSONException j){
            BFToast asdf = new BFToast(j.getMessage());
        }
    }

    @Override
    public  void onBackPressed(){
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in,R.anim.slide_down);
    }
}
