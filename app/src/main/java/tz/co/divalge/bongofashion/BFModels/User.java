package tz.co.divalge.bongofashion.BFModels;

/**
 * Created by Mwakalinga J on 2/23/2018.
 */

public class User {
    public int Id;
    public String Name;
    public String Address;
    public String Phonenumber;
    public String Bio;
    public String Email;
    public String UserType;
    public String ImageLink;
    public String PostCount;
}
