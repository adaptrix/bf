package tz.co.divalge.bongofashion.BFValues;

/**
 * Created by Mwakalinga J on 3/8/2018.
 */

public class BFConstants {
    public final static int STYLES_VALUE = 0;
    public final static int SUPERCATS_VALUE= 1;
    public final static int CATEGORIES_VALUE = 2;
    public final static int USERS_VALUE = 3;
}
