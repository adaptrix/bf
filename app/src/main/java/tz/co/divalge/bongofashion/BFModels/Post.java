package tz.co.divalge.bongofashion.BFModels;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mwakalinga J on 2/23/2018.
 */

public class Post {
    public int Id;
    public int UserId;
    public int CategoryId;
    public int StyleId;
    public String name;
    public String style;
    public String category;
    public String Description;
    public String postedBy;
    public String postTime;
    public List<String> ImageLinks = new ArrayList<String>();
}
