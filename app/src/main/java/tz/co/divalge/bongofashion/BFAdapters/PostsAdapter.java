package tz.co.divalge.bongofashion.BFAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import tz.co.divalge.bongofashion.BFActivites.MainActivity;
import tz.co.divalge.bongofashion.BFActivites.ViewPost;
import tz.co.divalge.bongofashion.BFHelpers.BFImageLoad;
import tz.co.divalge.bongofashion.BFModels.PostList;
import tz.co.divalge.bongofashion.BFValues.APIURls;
import tz.co.divalge.bongofashion.R;

/**
 * Created by Mwakalinga J on 2/23/2018.
 */

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.PostsViewHolder> implements View.OnClickListener {

    private List<PostList> posts;
    private Context context;
    private Activity activity;

    public class PostsViewHolder extends RecyclerView.ViewHolder{
        ImageView postImage;
        TextView postName, postTime, postedBy;
        CardView postContainer;
        public PostsViewHolder(View itemView) {
            super(itemView);
            postImage = itemView.findViewById(R.id.post_image);
            postName = itemView.findViewById(R.id.post_name);
            postedBy = itemView.findViewById(R.id.post_postedby);
            postTime = itemView.findViewById(R.id.post_time);
            postContainer = itemView.findViewById(R.id.post_container);
        }
    }

    public PostsAdapter(List<PostList> p, Context c,Activity a){
        posts = p;
        context = c;
        activity = a;
    }

    @Override
    public PostsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.post,parent,false);
        return new PostsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PostsViewHolder holder, int position) {
        final PostList post = posts.get(position);
        holder.postedBy.setText(post.postedBy);
        holder.postName.setText(post.name);
        holder.postTime.setText(post.postTime);
        holder.postContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ViewPost.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id",post.Id);
                intent.putExtra("name",post.name);
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.slide_up,R.anim.fade_out);
            }
        });
        if(post.postImage!=null&post.postImage!=""){
            BFImageLoad asdfadf= new BFImageLoad(holder.postImage,post.postImage,MainActivity.mainActivityContext,1);
        }

    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.post_container:
                break;
        }
    }
}
