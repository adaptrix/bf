package tz.co.divalge.bongofashion.BFAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import tz.co.divalge.bongofashion.BFActivites.MainActivity;
import tz.co.divalge.bongofashion.BFHelpers.BFImageLoad;
import tz.co.divalge.bongofashion.BFModels.CategoryModel;
import tz.co.divalge.bongofashion.BFValues.APIURls;
import tz.co.divalge.bongofashion.BFValues.BFConstants;
import tz.co.divalge.bongofashion.R;

/**
 * Created by Mwakalinga J on 2/23/2018.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> implements View.OnClickListener {

    private List<CategoryModel> categories;
    private Context context;
    private Activity activity;
    private int type;


    public class CategoryViewHolder extends RecyclerView.ViewHolder{
        ImageView categoryImage;
        TextView categoryName;
        CardView categoryContainer;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            categoryContainer = itemView.findViewById(R.id.category_container);
            categoryName = itemView.findViewById(R.id.category_name);
            categoryImage = itemView.findViewById(R.id.category_image);
        }
    }

    public CategoryAdapter(List<CategoryModel> c, Context ctx, Activity a, int t){
        this.categories = c;
        context = ctx;
        activity = a;
        type = t;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category,parent,false);
        return new CategoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        final CategoryModel category = categories.get(position);
        holder.categoryName.setText(category.Name);
        BFImageLoad fasdf = new BFImageLoad(holder.categoryImage,category.ImageLink,holder.categoryImage.getContext());

        switch (type){
            case BFConstants.CATEGORIES_VALUE:
                holder.categoryContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, tz.co.divalge.bongofashion.BFActivites.Category.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("id",category.Id);
                        context.startActivity(intent);
                        activity.overridePendingTransition(R.anim.slide_up,R.anim.fade_out);
                    }
                });
                break;
            case BFConstants.USERS_VALUE:
                holder.categoryContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, tz.co.divalge.bongofashion.BFActivites.Profile.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("id",category.Id);
                        context.startActivity(intent);
                        activity.overridePendingTransition(R.anim.slide_up,R.anim.fade_out);
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
        }
    }
}
