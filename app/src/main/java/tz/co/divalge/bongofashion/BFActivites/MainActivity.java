package tz.co.divalge.bongofashion.BFActivites;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import tz.co.divalge.bongofashion.BFAdapters.MainPagerAdapter;
import tz.co.divalge.bongofashion.BFFragments.Home.Home;
import tz.co.divalge.bongofashion.BFFragments.Home.HomeCategories;
import tz.co.divalge.bongofashion.BFFragments.Home.HomeStyles;
import tz.co.divalge.bongofashion.BFFragments.Home.HomeSuperCats;
import tz.co.divalge.bongofashion.BFFragments.Search.SearchFragment;
import tz.co.divalge.bongofashion.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public static Context mainActivityContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivityContext = this;
        toolbar = findViewById(R.id.main_toolbar);
        tabLayout = findViewById(R.id.main_tab_layout);
        viewPager = findViewById(R.id.main_view_pager);

        ImageView searchButton = findViewById(R.id.main_search_button);
        searchButton.setOnClickListener(this);

        initialiseTabLayout();
        initialiseViewPager();

        overridePendingTransition(R.anim.slide_left,R.anim.fade_out);
    }

    private void initialiseViewPager() {
        MainPagerAdapter mainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        mainPagerAdapter.addFragment(new Home());
        mainPagerAdapter.addFragment(new HomeCategories());
        mainPagerAdapter.addFragment(new HomeSuperCats());
        mainPagerAdapter.addFragment(new HomeStyles());

        viewPager.setOffscreenPageLimit(8);
        viewPager.setAdapter(mainPagerAdapter);
        viewPager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
    }

    private void initialiseTabLayout() {
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_tb_home_brown));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_tb_cats_brown));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_tb_sc_brown));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_tb_styles_brown));
    }

    @Override
    public void onBackPressed(){}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.main_search_button:
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                DialogFragment dialogFragment = new SearchFragment();
                dialogFragment.show(fragmentTransaction,"dialog");
                break;
        }
    }
}
