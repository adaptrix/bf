package tz.co.divalge.bongofashion.BFHelpers;

import android.content.Context;
import android.widget.Toast;

import tz.co.divalge.bongofashion.BFActivites.MainActivity;

/**
 * Created by Mwakalinga J on 2/23/2018.
 */

public class BFToast {
    public BFToast( String text){
        Toast.makeText(MainActivity.mainActivityContext,text,Toast.LENGTH_SHORT).show();
    }

    public BFToast(Context context, String text){
        Toast.makeText(context,text,Toast.LENGTH_SHORT).show();
    }
}
