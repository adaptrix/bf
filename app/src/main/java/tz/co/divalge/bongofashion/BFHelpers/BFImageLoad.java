package tz.co.divalge.bongofashion.BFHelpers;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import tz.co.divalge.bongofashion.BFValues.APIURls;
import tz.co.divalge.bongofashion.R;



/**
 * Created by Mwakalinga J on 3/8/2018.
 */

public class BFImageLoad {

    public BFImageLoad(ImageView image, String url, Context context){
        if(url!=null && !url.equals("") && !url.equals("null"))
        Picasso.with(context)
                .load(APIURls.IMAGE_PATH+url)
                .placeholder(R.drawable.ic_holder)
                .into(image);
        else
            Picasso.with(context)
                    .load(R.drawable.ic_no_image)
                    .into(image);
    }

    public BFImageLoad(ImageView image, String url, Context context,int i){
        if(url!=null && !url.equals("") && !url.equals("null"))
            Picasso.with(context)
                    .load(APIURls.IMAGE_PATH+url)
                    .resize(100,140)
                    .centerInside()
                    .placeholder(R.drawable.ic_holder)
                    .error(R.drawable.bongofashionicon)
                    .into(image);
            else
                Picasso.with(context)
                .load(R.drawable.ic_no_image)
                .into(image);
    }

    public BFImageLoad(ImageView image, String url, Context context,boolean b){
        if(url!=null && !url.equals("") && !url.equals("null"))
            Picasso.with(context)
                    .load(APIURls.IMAGE_PATH+url)
                    .resize(100,140)
                    .centerInside()
                    .placeholder(R.drawable.ic_holder)
                    .error(R.drawable.bongofashionicon)
                    .into(image);
        else
            Picasso.with(context)
                    .load(R.drawable.ic_user)
                    .into(image);
    }

    public BFImageLoad(ImageView image, Context context, String url){
        if(url!=null && !url.equals("") && !url.equals("null"))
            Picasso.with(context)
                    .load(APIURls.IMAGE_PATH+url)
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.bongofashionicon)
                    .into(image);
        else
            Picasso.with(context)
                    .load(R.drawable.ic_user)
                    .into(image);
    }
}
