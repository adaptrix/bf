package tz.co.divalge.bongofashion.BFValues;

/**
 * Created by Mwakalinga J on 2/23/2018.
 */

public class APIURls {
    private final static String SERVER = "http://bongofashion.co.tz/index.php/";
    private final static String API = "api/";

    public final static String IMAGE_PATH = "http://bongofashion.co.tz/storage/";

    public final static String POSTS = SERVER+API+"posts/latest";
    public final static String SUPERCATS = SERVER+API+"supercats/";
    public final static String STYLES = SERVER+API+"styles/";
    public final static String USERCATS = SERVER+API+"usercats";

    public final static String USERS_POSTS_USERID = SERVER+API+"posts/user/";
    public final static String POST_DETAILS_POSTID = SERVER+API+"post/";
    public final static String POSTS_SUTPERCATS_SUPERCATID = SERVER+API+"posts/supercats/";
    public final static String POSTS_STYLES_STYLEID = SERVER+API+"posts/style/";
    public final static String POSTS_USERCATS_USERCATID = SERVER+API+"posts/usercats/";

    public final static String USERS_FROM_SUPERCAT_SUEPERCATID = SERVER+API+"users/supercats/";
    public final static String USER_PROFILE_USERID = SERVER+API+"users/";

    public final static String FILTERS = SERVER + API + "filters/";
    public final static String SEARCH = SERVER + API + "search";
}
