package tz.co.divalge.bongofashion.BFAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import tz.co.divalge.bongofashion.BFActivites.MainActivity;
import tz.co.divalge.bongofashion.BFHelpers.BFImageLoad;
import tz.co.divalge.bongofashion.BFModels.Style;
import tz.co.divalge.bongofashion.BFValues.APIURls;
import tz.co.divalge.bongofashion.BFValues.BFConstants;
import tz.co.divalge.bongofashion.R;

/**
 * Created by Mwakalinga J on 2/23/2018.
 */

public class StyleAdapter extends RecyclerView.Adapter<StyleAdapter.StyleViewHolder> implements View.OnClickListener{

    private List<Style> styles;
    private Context context;
    private Activity activity;
    private String url;
    public class StyleViewHolder extends RecyclerView.ViewHolder{
        ImageView styleImage;
        TextView styleName;
        CardView styleContainer;

        public StyleViewHolder(View itemView) {
            super(itemView);
            styleImage = itemView.findViewById(R.id.style_image);
            styleContainer = itemView.findViewById(R.id.style_container);
            styleName = itemView.findViewById(R.id.style_name);
        }
    }

    public StyleAdapter(List<Style> s, Context ctx, Activity a, int t){
        this.styles = s;
        context = ctx;
        activity = a;
        switch (t){
            case BFConstants.STYLES_VALUE:
                url = APIURls.POSTS_STYLES_STYLEID;
                break;
            case BFConstants.SUPERCATS_VALUE:
                url = APIURls.POSTS_SUTPERCATS_SUPERCATID;
                break;
        }
    }

    @Override
    public StyleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.style,parent,false);
        return new StyleViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StyleViewHolder holder, int position) {
        final Style style = styles.get(position);
        holder.styleName.setText(style.Name);
        holder.styleContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, tz.co.divalge.bongofashion.BFActivites.Style.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("name",style.Name);
                intent.putExtra("url",url+style.Id);
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.slide_up,R.anim.fade_out);
            }
        });
        if(style.ImageLink!=null&style.ImageLink!=""){
            BFImageLoad adffd = new BFImageLoad(holder.styleImage,style.ImageLink,holder.styleImage.getContext());
        }
    }

    @Override
    public int getItemCount() {
        return styles.size();
    }

    @Override
    public void onClick(View v) {
    }

}
